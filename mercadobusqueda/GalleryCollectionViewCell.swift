//
//  GalleryCollectionViewCell.swift
//  mercadobusqueda
//
//  Created by Leandro Lopez on 20/05/2022.
//

import UIKit
import AlamofireImage

class GalleryCollectionViewCell: UICollectionViewCell {

    @IBOutlet var imageView: UIImageView!

    static let cellIdentifier = "GalleryCollectionViewCell"

    func configure(urlString: String) {
        if let url = URL(string: urlString), let placeholder = UIImage(named: "GrayPlaceholder") {
            imageView.af.setImage(withURL: url, placeholderImage: placeholder, imageTransition: .crossDissolve(0.2))
       }
    }
}

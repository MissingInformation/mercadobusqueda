//
//  InfoModel.swift
//  mercadobusqueda
//
//  Created by Leandro Lopez on 23/05/2022.
//

import UIKit

enum InfoType {
    case readyToSearch
    case noResults
    case searchError
    case connectionFailure
}

struct InfoModel {
    let topText: String
    let bottomText: String
    let imageName: String
}

struct ItemInfo {
    let title: String
    let price: String
    let sellerId: String
    let elementId: String
}

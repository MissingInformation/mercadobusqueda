//
//  DetailViewController.swift
//  mercadobusqueda
//
//  Created by Leandro Lopez on 19/05/2022.
//

import Alamofire
import UIKit

class DetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var sellerNicknameLabel: UILabel!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var quickInfoLabel: UILabel!
    @IBOutlet weak var grayView: UIView!
    @IBOutlet weak var attributesLabel: UILabel!
    
    static let detailSegueIdentifier = "DetailViewControllerSegue"
    
    var itemInfo: ItemInfo?
    
    var images = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = PagingCollectionViewLayout()
        
        if UIScreen.isPortrait {
            layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
            
        } else {
            layout.itemSize = CGSize(width: UIScreen.main.bounds.height, height: UIScreen.main.bounds.height)
        }
        
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        galleryCollectionView.collectionViewLayout = layout
        galleryCollectionView.delegate = self
        galleryCollectionView.dataSource = self
        
        if let info = itemInfo {
            requestItemDetails(itemInfo: info)
            titleLabel.text = info.title
            priceLabel.text = info.price
        } else {
            errorView.isHidden = false
        }
        
        grayView.layer.cornerRadius = 5
        titleView.layer.cornerRadius = 10
    }
    
    func showError() {
        print("\(self.nibName ?? "") error")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    fileprivate func parsePictures(_ itemInformation: [String: Any]) {
        if let pictures = itemInformation["pictures"] as? [[String: Any]] {
            for picture in pictures {
                if let pictureUrl = picture["secure_url"] as? String {
                    self.images.append(pictureUrl)
                }
            }
            self.galleryCollectionView.reloadData()
        }
    }
    
    fileprivate func parseAddress(_ itemInformation: [String: Any]) {
        if let seller = itemInformation["seller_address"] as? [String: Any] {
            guard let state = seller["state"] as? [String: Any],
                  let stateName = state["name"] as? String,
                  let city = seller["city"] as? [String: Any],
                  let cityName = city["name"] as? String else { return }
            quickInfoLabel.text! += "Ubicación: \(cityName), \(stateName)\n"
        }
    }
    
    fileprivate func parseWarranty(_ itemInformation: [String: Any]) {
        if let warranty = itemInformation["warranty"] as? String {
            quickInfoLabel.text! += "Garantía: \(warranty)\n"
        }
    }
    
    fileprivate func parseAttributes(_ itemInformation: [String: Any]) {
        var attributesArray = [String]()
        if let attributes = itemInformation["attributes"] as? [[String: Any]] {
            for attribute in attributes {
                guard let attributeName = attribute["name"] as? String,
                      let attributeValue = attribute["value_name"] as? String else { continue }
                attributesArray.append("\(attributeName): \(attributeValue)")
            }
        }
        attributesLabel.text = attributesArray.joined(separator: "\n")
    }
    
    fileprivate func parseSellerNickname(_ data: Data) {
        if let sellerInformation = try? JSONSerialization.jsonObject(with: data,
                                                                     options: .mutableContainers) as? [String: Any] {
            guard let nickname = sellerInformation["nickname"] else { return }
            quickInfoLabel.text! += "Vendedor: \(nickname)\n"
        }
    }
    
    fileprivate func requestItemDetails(itemInfo: ItemInfo) {
        AF.request(Api.itemPath+itemInfo.elementId+Api.itemDescrptionAttributes, method: .get).response { response in
            switch response.result {
            case .success:
                guard let data = response.data else { return }
                if let itemDescriptionResponse = try? JSONSerialization.jsonObject(
                    with: data, options: .mutableContainers) as? [String: Any] {
                    if let descriptionText = itemDescriptionResponse["plain_text"] as? String {
                        self.descriptionLabel.text = descriptionText
                        self.descriptionLabel.sizeToFit()
                    } else {
                        // should I use en error message in this label?
                        self.descriptionLabel.text = ""
                    }
                }
            case let .failure(error):
                print("failure", error)
            }
        }
        
        AF.request(Api.sellerPath+itemInfo.sellerId+Api.sellerAttributes, method: .get).response { response in
            switch response.result {
            case .success:
                guard let data = response.data else { return }
                self.parseSellerNickname(data)
            case let .failure(error):
                print("failure", error)
            }
        }
        
        AF.request(Api.itemPath+itemInfo.elementId+Api.itemInfoAttributes, method: .get).response { response in
            switch response.result {
            case .success:
                guard let data = response.data else { return }
                if let itemInformation = try? JSONSerialization.jsonObject(
                    with: data, options: .mutableContainers) as? [String: Any] {
                    self.parsePictures(itemInformation)
                    self.parseWarranty(itemInformation)
                    self.parseAddress(itemInformation)
                    self.parseAttributes(itemInformation)
                }
            case let .failure(error):
                print("failure", error)
            }
        }
    }
    
    // MARK: - CollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryCollectionViewCell.cellIdentifier,
                                                      for: indexPath) as! GalleryCollectionViewCell
        cell.configure(urlString: images[indexPath.row])
        return cell
    }
    
}

//
//  ResultModels.swift
//  mercadobusqueda
//
//  Created by Leandro Lopez on 23/05/2022.
//

import UIKit

struct ResultModel {
    let elementId: String
    let title: String
    let price: String
    let thumbnail: String
    let sellerId: String
}

struct ResultCellViewModel {
    let elementId: String
    let title: String
    let price: String
    let thumbnail: String
    let sellerNickname: String
}

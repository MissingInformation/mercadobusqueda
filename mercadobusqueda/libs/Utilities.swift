//
//  Utilities.swift
//  mercadobusqueda
//
//  Created by Leandro Lopez on 23/05/2022.
//

import UIKit

extension UIScreen {
    static var isPortrait: Bool { return UIScreen.main.bounds.height > UIScreen.main.bounds.width }
}

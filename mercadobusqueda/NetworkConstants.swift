//
//  NetworkConstants.swift
//  mercadobusqueda
//
//  Created by Leandro Lopez on 23/05/2022.
//

import UIKit

enum Api {
   static let host = "https://api.mercadolibre.com/"
   static let searchPath = host + "sites/MLA/search?q="
   static let itemPath = host + "items/"
   static let itemInfoAttributes = "?attributes=pictures,seller_address,warranty,attributes"
   static let itemDescrptionAttributes = "/description?attributes=plain_text"
   static let sellerPath = host + "users/"
   static let sellerAttributes = "?attributes=nickname"
}

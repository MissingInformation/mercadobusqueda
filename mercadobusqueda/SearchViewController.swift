//
//  ViewController.swift
//  mercadobusqueda
//
//  Created by Leandro Lopez on 19/05/2022.
//

import UIKit
import Alamofire

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var infoView: InfoView!

    var searchResultModels = [ResultModel]()
    var resultCellViewModels = [ResultCellViewModel]()

    let searchController = UISearchController(searchResultsController: nil)

    fileprivate func infoScreen(willShow: Bool) {
        UIView.transition(with: self.view, duration: 0.1, options: .transitionCrossDissolve) {
            self.infoView.isHidden = !willShow
        }
    }

    fileprivate func showInfo(type: InfoType, message: String = "") {
        switch type {
        case .readyToSearch:
            infoView.configure(InfoModel(topText: "Busca productos, marcas y más...",
                                         bottomText: "",
                                         imageName: "MagnifyingGlass"))
        case .noResults:
            infoView.configure(InfoModel(topText: "No encontramos publicaciones que coincidan con tu búsqueda.",
                                         bottomText: "Revisa que la misma esté bien escrita.",
                                         imageName: "NoResults"))
        case .searchError:
            infoView.configure(InfoModel(topText: "Ha ocurrido un error con tu búsqueda.",
                                         bottomText: "Inténtalo nuevamente en unos minutos.",
                                         imageName: "SearchError"))
        case .connectionFailure:
            infoView.configure(InfoModel(topText: "Ha ocurrido un error.",
                                         bottomText: "Inténtalo nuevamente en unos minutos.",
                                         imageName: "ConnectionFailure"))
        }
        infoScreen(willShow: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        searchController.searchBar.placeholder = "Buscar"

        self.navigationItem.searchController = searchController
        self.navigationItem.searchController?.searchBar.delegate = self
        self.navigationController?.hidesBarsOnSwipe = true

        tableView.delegate = self
        tableView.dataSource = self

        self.showInfo(type: .readyToSearch)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == DetailViewController.detailSegueIdentifier {

            if let indexPath = tableView.indexPathForSelectedRow {
                let searchresultmodel = searchResultModels[indexPath.row]
                if let detailViewcontroller = segue.destination as? DetailViewController {
                    detailViewcontroller.itemInfo = ItemInfo(title: searchresultmodel.title,
                                                             price: searchresultmodel.price,
                                                             sellerId: searchresultmodel.sellerId,
                                                             elementId: searchresultmodel.elementId)
                }
            }
        }
    }

    // MARK: - search

    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        infoScreen(willShow: false)
        return true
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchResultModels.removeAll()
        resultCellViewModels.removeAll()
        tableView.reloadData()
        showInfo(type: .readyToSearch)
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if let searchText = searchBar.text, searchText.isEmpty == false {
            infoScreen(willShow: false)
            searchResultModels.removeAll()
            resultCellViewModels.removeAll()
            tableView.reloadData()
            searchTerms(searchText)
        }
    }

    fileprivate func searchTerms(_ terms: String) {

        let cterms = terms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

        AF.request(Api.searchPath+cterms, method: .get).response { response in
            switch response.result {
            case .success:
                guard let data = response.data else {
                    self.showInfo(type: .searchError)
                    return
                }
                if let results = try? JSONSerialization.jsonObject(with: data,
                                                                   options: .mutableContainers) as? [String: Any] {
                    self.loadTable(with: results)
                } else {
                    self.showInfo(type: .noResults)
                }
            case let .failure(error):
                self.showInfo(type: .connectionFailure, message: error.localizedDescription)
            }
        }
    }

    // MARK: - tableView

    func loadTable(with dictionary: [String: Any]) {
        if let results = dictionary["results"] as? [[String: Any]], results.isEmpty != true {

            for element in results {
                if let elementId = element["id"] as? String,
                   let title = element["title"] as? String,
                   let price = element["price"]as? Double,
                   let thumbnail = element["thumbnail"]as? String,
                   let seller = element["seller"] as? [String: Any],
                   let sellerId = seller["id"] as? Int {

                    let formatter = NumberFormatter()

                    formatter.minimumFractionDigits = 0
                    formatter.maximumFractionDigits = 2

                    let fractionDigits = formatter.string(from: NSNumber(value: price)) ?? String(price)

                    let formatedPrice = "$ \(fractionDigits)"

                    // Using a Quick and Dirty string replacement for the thumbnail url scheme.
                    let model = ResultModel(elementId: elementId, title: title,
                                            price: formatedPrice,
                                            thumbnail: thumbnail.replacingOccurrences(of: "http:", with: "https:"),
                                            sellerId: String(sellerId))
                    searchResultModels.append(model)

                    resultCellViewModels.append(ResultCellViewModel(elementId: model.elementId,
                                                                    title: model.title,
                                                                    price: formatedPrice,
                                                                    thumbnail: model.thumbnail,
                                                                    sellerNickname: model.sellerId))

                }
            }
            tableView.reloadData()
        } else {
            showInfo(type: .noResults)
        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultCellViewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultTableViewCell.cellIdentifier,
                                                 for: indexPath) as! SearchResultTableViewCell
        cell.configure(model: resultCellViewModels[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
}

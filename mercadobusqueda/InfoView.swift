//
//  InfoView.swift
//  mercadobusqueda
//
//  Created by Leandro Lopez on 23/05/2022.
//

import UIKit

class InfoView: UIView {

    @IBOutlet var topLabel: UILabel!
    @IBOutlet var bottomLabel: UILabel!
    @IBOutlet var imageView: UIImageView!

    func configure(_ model: InfoModel) {
        self.topLabel.text = model.topText
        self.bottomLabel.text = model.bottomText
        self.imageView.image = UIImage(named: model.imageName)
    }
}

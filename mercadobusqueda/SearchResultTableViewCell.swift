//
//  SearchResultTableViewCell.swift
//  mercadobusqueda
//
//  Created by Leandro Lopez on 19/05/2022.
//

import UIKit
import AlamofireImage
class SearchResultTableViewCell: UITableViewCell {

    static let cellIdentifier = "SearchResultTableViewCell"

    @IBOutlet var title: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var info: UILabel!
    @IBOutlet var thumbnail: UIImageView!

    var thumbnailUrl = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        thumbnail.layer.cornerRadius = 5
    }

    func configure(model: ResultCellViewModel) {
        self.title.text = model.title
        self.price.text = model.price
        self.thumbnailUrl = model.thumbnail

        self.title.sizeToFit()
        self.price.sizeToFit()
        self.info.sizeToFit()

        // Quick and Dirty string replacement
        if let url = URL(string: model.thumbnail), let placeholder = UIImage(named: "GrayPlaceholder") {
            thumbnail.af.setImage(withURL: url, placeholderImage: placeholder, imageTransition: .crossDissolve(0.2))
       }
    }
}

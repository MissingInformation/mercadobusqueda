//
//  mercadobusquedaUITests.swift
//  mercadobusquedaUITests
//
//  Created by Leandro Lopez on 19/05/2022.
//

import XCTest

class mercadobusquedaUITests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    func testEmptySearchResult() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
         
        var topLabelText = app.staticTexts["infoScreenTopLabel"].label
        XCTAssert(topLabelText == "Busca productos, marcas y más...")

        let searchView = app.navigationBars["mercadobusqueda.SearchView"]
        searchView.searchFields["Buscar"].tap()
        searchView.searchFields["Buscar"].typeText("AnImpossibleSearch\n")
                
        topLabelText = app.staticTexts["infoScreenTopLabel"].label
        XCTAssert(topLabelText == "No encontramos publicaciones que coincidan con tu búsqueda.")

        searchView.buttons["Cancel"].tap()

        topLabelText = app.staticTexts["infoScreenTopLabel"].label
        XCTAssert(topLabelText == "Busca productos, marcas y más...")
    }
    
    func testEmptySearchResultLAndscapeTest() throws {
        // UI tests must launch the application that they test.
        
        let app = XCUIApplication()
        app.launch()
        
        XCUIDevice.shared.orientation = .landscapeLeft
        
        var topLabelText = app.staticTexts["infoScreenTopLabel"].label
        XCTAssert(topLabelText == "Busca productos, marcas y más...")

        let searchView = app.navigationBars["mercadobusqueda.SearchView"]
        searchView.searchFields["Buscar"].tap()
        searchView.searchFields["Buscar"].typeText("AnImpossibleSearch\n")
                
        topLabelText = app.staticTexts["infoScreenTopLabel"].label
        XCTAssert(topLabelText == "No encontramos publicaciones que coincidan con tu búsqueda.")

        searchView.buttons["Cancel"].tap()

        topLabelText = app.staticTexts["infoScreenTopLabel"].label
        XCTAssert(topLabelText == "Busca productos, marcas y más...")
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
